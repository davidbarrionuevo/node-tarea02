
const fs =  require('fs')
//import * as fs from 'fs'
const nroTabla=5
const directorio="."
const nombrePrefijo=`tabla_del_${nroTabla}`
const nombreExtension=".txt"
const fecha = new Date()
const saltoLlinea="\n"
let tabla=`Tabla del ${nroTabla}${saltoLlinea}`

for (let i=1; i<=10; i++){
    let lineaTabla=`${nroTabla} x ${i} = ${i * nroTabla}${saltoLlinea}`
    tabla=tabla+lineaTabla
}
//console.log(tabla)
const hoy= fecha.getDate()
const mls= fecha.getTime()
let nombreArchivo=`${nombrePrefijo}_${hoy}-${mls}_${nombreExtension}`
fs.writeFile(nombreArchivo, tabla,{encoding:"utf8"}, (e) => {
    if (e) console.log(e)
    else {
        console.log("Archivo Grabado Exitosamente."+ saltoLlinea)
        console.log(`El archivo ${nombreArchivo} contiene:`)
        setTimeout(() => {
            console.log(fs.readFileSync(nombreArchivo, "utf8"))
        }, 3000)
    }
})
